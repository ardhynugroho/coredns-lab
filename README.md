# CoreDNS for DNS DoS Lab

NGINX Ingress Controller (IC) configuration to serve DNS using UDP load balancer.

```
Client --> NODEPORT:30053 --> NGINX-UDP:5353 --> CoreDNS-Pods:53
```

Configuration related to NGINX IC:

1. Define `dns-udp` listener in global configuration
2. create TransportServer resource type. 
This resource is used to create load balancer configuration in stream server context, listening on port 5353
3. Expose the service using node port, manually mapping port 5353 in the NGINX to port 30053 in the host.

```
kubectl apply -f dns.yaml,gc.yaml,ts-udp.yaml,service.yaml
```

## Docker Version

Deploy CoreDNS

```
docker compose up -d
```

Deploy dnsperf in other VM

```
docker run -it --name dnsperf nicolaka/netshoot sh
apk update
apk install dnsperf
echo "example.com a" > example.com
```

Test the CoreDNS server

```
dnsperf -s COREDNS_IPADDR -p 55553 -d example.com -c 30 -T 3o -t 30 -l 30 -q 5000 -Q 3000
```
